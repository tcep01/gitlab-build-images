#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

export DEBIAN_FRONTEND=noninteractive

# Install LaTeX and pandoc 2.3.1
apt-get update
apt-get install -yq --no-install-recommends \
  make gcc g++ locales \
  rsync git-core texlive-latex-recommended texlive-xetex \
  texlive-fonts-recommended lmodern ed file curl gnupg2 \

cd /tmp
curl -L -O https://github.com/jgm/pandoc/releases/download/2.3.1/pandoc-2.3.1-linux.tar.gz
tar xvf pandoc-2.3.1-linux.tar.gz
cp pandoc-2.3.1/bin/* /usr/local/bin
rm -rf /tmp/pandoc*

# For cropping the pictures on the team page
apt-get install -yq --no-install-recommends imagemagick

# Installing node & yarn
NODE_INSTALL_VERSION=8.x
YARN_INSTALL_VERSION=1.2.1-1
/scripts/install-node $NODE_INSTALL_VERSION $YARN_INSTALL_VERSION && node --version && yarn --version

# Installing gitlab-runner
curl -O -J -L https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64
mv gitlab-ci-multi-runner-linux-amd64 /usr/bin/gitlab-runner-helper
chmod +x /usr/bin/gitlab-runner-helper

# Set UTF-8
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 LC_ALL=en_US.UTF-8
locale -a

apt-get autoremove -yq
apt-get clean -yqq
rm -rf /var/lib/apt/lists/*
