FROM ruby:2.4.5-alpine
MAINTAINER GitLab Distribution Team

ENV HELM_VERSION=2.12.3
ENV HELM_URL=https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz

# kubectl (possibly in gcloud?)
ENV KUBECTL_VERSION=1.9.10
ENV KUBECTL_URL=https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl

# Install dependencies
RUN apk --no-cache add -U openssl curl tar gzip bash ca-certificates git python2\
  && mkdir -p /opt

# Install kubectl
RUN curl -L -o /usr/local/bin/kubectl ${KUBECTL_URL} \
  && chmod +x /usr/local/bin/kubectl \
  && kubectl version --client

# Install Helm
RUN wget -q -O - ${HELM_URL} | tar zxf - \
    && mv linux-amd64/helm /usr/bin/ \
    && chmod +x /usr/bin/helm \
    && helm version --client
