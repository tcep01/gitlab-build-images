# GitLab Build Images

This repository is used to build docker images that are used to build and test
various parts of GitLab:

1. Build Omnibus packages
1. Test GitLab-CE/EE project
1. Build gitlab-org/www-gitlab-com project

## Adding a new image

There are two methods to adding a new image:

1. Use a docker buildfile.
1. Use a custom image with versioned features.

### Use a docker buildfile

A bespoke image is one that uses it's own Dockerfile. In the interests of
avoiding repetition, it's better to use the custom docker build if possible.

Assuming the image you want to add is called `new-image`:

1. Add a test task: `new-image test: *test_build`.
1. Add a new build task: `new-image: *build_and_deploy`.
1. Add a Dockerfile: `Dockerfile.new-image`.

### Use a custom image with versioned features

To add a new image to the build, edit [`.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab-build-images/blob/master/.gitlab-ci.yml).

With custom images, the name of the image is used to define the contents (as opposed to use a bespoke Dockerfile build).

For this reason, the name needs to be correctly defined.

The name is defined as `[feature]-[version]` pairs, separated by `-`.

Here are some example build names:

1. `ruby-2.1-git-2.7`
1. `ruby-2.3.3-golang-1.8-git-2.7-chrome-60.0-node-7.1-postgresql-9.6`
1. `ruby-2.4.1-golang-1.8-git-2.7-node-7.1-postgresql-9.6`

The first pair represents the base image and version. So `ruby-2.1-...` will use the `ruby:2.1` base image, while
`golang-1.8` will use the `golang:1.8` base image.

Each of the following parts represents another feature and version. Available
options are:

1. `golang`
1. `git`
1. `chrome`
1. `node`
1. `yarn`
1. `postgres`
1. `terraform`
1. `ansible`
1. `graphicsmagick`

#### Adding a new build

As an example, if you want to add new image for Ruby 2.4 with `git` 2.14 and `golang` 1.9,
the name would be `ruby-2.4-golang-1.9-git-2.14`.

1. Add a test task: `ruby-2.4-golang-1.9-git-2.14 test: *test_custom`
1. Add a new build task: `ruby-2.4-golang-1.9-git-2.14: *build_and_deploy_custom`

## Note regarding Google Chrome

Google has a policy of yanking older versions of Google Chrome from their PPA
whenever a new major version is released. To help maintain consistent build
images, there is a CI step that saves the latest Google Chrome .deb into an S3
bucket. The install for Chrome will attempt to retrieve from the bucket if it
cannot find the file in the apt repository. See `scripts/cache-google-chrome`
for more details.

## Contributing

See [Contributing](CONTRIBUTING.md)
