# Used by:
#  - GitLab CE/EE for QA: https://gitlab.com/gitlab-org/gitlab-ee/blob/master/.gitlab-ci.yml
#  - Nightly QA: https://gitlab.com/gitlab-org/quality/nightly/blob/master/.gitlab-ci.yml
#  - Staging QA: https://gitlab.com/gitlab-org/quality/staging/blob/master/.gitlab-ci.yml

FROM ruby:2.5.3-alpine
MAINTAINER GitLab Quality Team

WORKDIR /home/qa

ENV DOCKER_VERSION="18.06.1"
ENV DOCKER_ARCHIVE="docker-${DOCKER_VERSION}-ce.tgz"
ENV DOCKER_URL="https://download.docker.com/linux/static/stable/x86_64/${DOCKER_ARCHIVE}"
ENV DOCKER_CHECKSUM="2d92728714d794f78619785a2c638b58b0b15e60b340de51886bf6d3730f31f0"
ENV DOCKER_BIN="/usr/local/bin/docker"

# Install dependencies
RUN apk --no-cache add -U openssl tar gzip bash ca-certificates && mkdir -p /opt

RUN wget -q ${DOCKER_URL} && \
    tar -zxf ${DOCKER_ARCHIVE} && \
    mv docker/docker ${DOCKER_BIN} && \
    echo "${DOCKER_CHECKSUM}  ${DOCKER_BIN}" | sha256sum -c - && \
    rm ${DOCKER_ARCHIVE}
